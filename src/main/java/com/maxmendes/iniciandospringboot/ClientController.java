package com.maxmendes.iniciandospringboot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maxmendes.iniciandospringboot.models.Greeting;

@RestController
public class ClientController {
	
	private static final String template = "Hello, %s!";

	
	@GetMapping("/")
	public Greeting greeting(
			@RequestParam(value = "name", defaultValue = "User") String name,
			@RequestParam(value = "id") Integer id) {
		
		Greeting greet = new Greeting(id, String.format(template, name));
		
		return greet;
	}
}
