package com.maxmendes.iniciandospringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IniciandoSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(IniciandoSpringbootApplication.class, args);
	}

}
